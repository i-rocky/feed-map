<?php

include $_SERVER['DOCUMENT_ROOT'].'/lib/Config.php';
$config = new Config;

if(preg_match('#^\/ajax\?t=\d+$#', $_SERVER['REQUEST_URI'])) {
	print json_encode([
		'lng' => $config->getConfig(Config::LNG_KEY),
		'lat' => $config->getConfig(Config::LAT_KEY)
	]);
	exit;
}
?>

<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Map Live Feed</title>
      <link rel="icon" href="/images/icon.ico" type="image/x-icon">
      <link rel="stylesheet" href="/css/styles.css" type="text/css">
  </head>
  <body>
    <div class="button">
        <div class="dropdown" id="dropdown">
            <label class="switch">
                <input type="checkbox" id="street-points" checked>
                <span class="slider round"></span>
            </label>
              <button id="re-render">Re-render</button>
        </div>
        <button id="drop-action"><img src="/images/arrow.png"></button>
    </div>
    <div id="map"></div>
    <script>
        var position = [<?=$config->getConfig(Config::LAT_KEY) ?>, <?= $config->getConfig(Config::LNG_KEY) ?>];
    </script>
    <script src="/js/script.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=$config->getConfig(Config::MAP_KEY)?>&callback=initMap"></script>
  </body>
</html>
