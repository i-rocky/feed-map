<?php

//AIzaSyDWLYat0vA7jljQ8fQdeAQwPJC4RWofm7w
class Config {

	private $config_file;
	private $config = [
		'google_map_api_key' => '',
		'secret' => '',
		'lng' => '',
		'lat' => ''
	];
	const MAP_KEY = 'google_map_api_key';
	const SECRET = 'secret';
	const LNG_KEY = 'lng';
	const LAT_KEY = 'lat';

	public function __construct()
	{
		$this->config_file = $_SERVER["DOCUMENT_ROOT"].'./data/config.json';
		$this->read();
	}

	private function read() {
		if(file_exists($this->config_file) && is_readable($this->config_file)) {
			$this->config = json_decode(file_get_contents($this->config_file), 1);
		}
	}

	private function write() {
		if(!@file_put_contents($this->config_file, json_encode($this->config))) {
			print 'Could not write file '.$this->config_file.'. chmod the directory "data" to 0777';
		}
	}

	public function getConfig($key=null) {
		if($key===null)
			return $this->config;
		return $this->config[$key];
	}

	public function setConfig($key, $value) {
		$this->config[$key] = $value;
	}

	public function __destruct()
	{
		$this->write();
	}

	public function __toString()
	{
		return "Google Map API Key: {$this->config[self::MAP_KEY]}<br/>Longitude: {$this->config[self::LNG_KEY]}<br/>Latitude: {$this->config[self::LAT_KEY]}";
	}
}