<?php

global $config;

//change credentials, keep this format ['username' => 'password']

$users = ['admin' => 'admin'];

if(isset($_POST['username'], $_POST['password']) && array_key_exists($_POST['username'], $users) && in_array($_POST['password'], $users)) {
	$_SESSION['logged_in'] = true;
}

if(!isset($_SESSION['logged_in'])) {
	?>

	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Live Map Feed Administration</title>
		<!--FAVICON ICON-->
		<link rel="icon" href="/images/icon.ico" type="image/x-icon">
		<link rel="stylesheet" href="/css/styles-admin.css" type="text/css">
		<!--GOOGLE FONTS-->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,500,700,900' rel='stylesheet'
		      type='text/css'>
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<div class="main">
		<div class="content">
			<div class="header">
				<h2>Authenticate</h2>
			</div>
			<div class="container">
				<div class="information">
					<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
						<div class="group">
							<label>Username</label>
							<p><input type="text" name="username"></p>
						</div>
						<div class="group">
							<label>Password</label>
							<p><input type="password" name="password"></p>
						</div>
						<div class="group">
							<p><input type="submit" value="Login"></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="/js/script-admin.js"></script>
	</body>
	</html>

<?php
	die();
}
?>