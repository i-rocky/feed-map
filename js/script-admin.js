var els = {};

apply(el('.main'), function (e) {
    e.style.height = (window.innerHeight-40) + "px";
    e.style.width = (window.innerWidth-40) + "px";
});
hideInput(true);
apply(el('#edit'), function (e) {
    e.onclick = function (e) {
        _preventDefault(e);
        hideInput(false);
    };
});
apply(el('#cancel'), function (e) {
    e.onclick = function (e) {
        _preventDefault(e);
        hideInput(true);
    };
});
function hideInput(bool) {
    apply(el('.input'), function (e) {
        e.style.display = bool?'none':'';
    });
    apply(el('.info'), function (e) {
        e.style.display =!bool?'none':'';
    });
}
function _preventDefault(e) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();
}
function apply(els, callback) {
    els.forEach(function (el) { callback.call(this, el); })
}
function el(s, first) {
    if(Object.keys(els).indexOf(s)>-1) {
        return first?els[s][0]:els[s];
    }
    els[s] = document.querySelectorAll(s);
    return first?els[s][0]:els[s];
}