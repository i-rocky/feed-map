var els = [];
function resize() {
    apply(el('#map'), function(e){
        e.style.height = (window.innerHeight - 20) + "px";
        e.style.width = (window.innerWidth - 20) + "px";
    });
    apply(el('.dropdown'), function (e) {
        e.style.width = (window.innerWidth - 20) + "px";
        e.style.overflow = 'hidden';
    });
    apply(el('.button'), function (e) {
        e.style.top = '-80px';
    });
    apply(el('#drop-action img'), function (e) {
        e.style.transform = 'rotate(180deg)';
    });
    apply(el('.dropdown button'), function (e) {
        e.style.marginRight = window.innerWidth < 500 ? '5%' : '23%';
    });
    apply(el('#street-points'), function (e) {
        e.checked = getCookie("street-points")!=="no";
    });
}
apply(el('#re-render'), function (e) {
    e.onclick = function () {
        calculateAndDisplayRoute();
        toggleDropDown();
    };
});
apply(el('#drop-action'), function (e) {
    e.onclick = function () {
        toggleDropDown();
    };
});
apply(el('#street-points'), function (e) {
    e.onclick = function () {
        setCookie("street-points", (e.checked ? "checked" : "no"), 30);
    };
});
function apply(els, callback) {
    els.forEach(function (el) { callback.call(this, el); })
}
function el(s, first) {
    if(Object.keys(els).indexOf(s)>-1) {
        return first?els[s][0]:els[s];
    }
    els[s] = document.querySelectorAll(s);
    return first?els[s][0]:els[s];
}
function toggleDropDown() {
    var dropdown = el('.button', true);
    var img = el('#drop-action img', true);
    var show = dropdown.style.top === '-80px';
    img.style.transform = show ? 'rotate(0deg)' : 'rotate(180deg)';
    dropdown.style.top = show ? '0' : '-80px';
}
function setCookie(cname, cvalue, exdays) {
    try {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    } catch (e) {
        console.log(e);
    }
}
function getCookie(cname) {
    try {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    } catch (e) {
        console.log(e);
    }
}
window.onresize = resize;
resize();

var map = undefined;
var remote_marker = undefined;
var my_marker = undefined;
var xmlhttp = undefined;

var directionService = undefined;
var directionDisplay = undefined;
var stepDisplay = undefined;
var markerArray = [];


function initMap() {
    var latlng = new google.maps.LatLng(position[0], position[1]);
    var options = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), options);
    map.panTo(latlng);
    setRemoteMarker(latlng);
    setMyMarker();
}

function initDirectionDisplay() {
    directionService = new google.maps.DirectionsService;
    directionDisplay = new google.maps.DirectionsRenderer({map: map, suppressMarkers: true});
    stepDisplay = new google.maps.InfoWindow;
    calculateAndDisplayRoute();
}

function setRemoteMarker(location) {
    remote_marker = makeMarker(location, 'Shuttle Bus');
    remote_marker.setIcon("/images/shuttle-bus.png");
    zoomToBound();
    setInterval(function () {
        getPos(function (position) {
            var pos = new google.maps.LatLng(position.lat, position.lng);
            remote_marker.setPosition(pos);
        });
    }, 10000);
}

function setMyMarker() {
    navigator.geolocation.watchPosition(function (position) {
        position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        if(my_marker===undefined) {
            my_marker = makeMarker(position, "You");
            my_marker.setIcon("/images/me.png");
            zoomToBound();
        }
        my_marker.setPosition(position);
    }, function(err) {
        console.log(err);
    });
}

function zoomToBound() {
    if(remote_marker===undefined || my_marker===undefined)
        return;
    var bounds = new google.maps.LatLngBounds(remote_marker.getPosition(), my_marker.getPosition());
    map.fitBounds(bounds);
    initDirectionDisplay();
}

function makeMarker(location, title) {
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: title
    });
    attachInfoWindow(marker, title);
    return marker;
}

function attachInfoWindow(marker, title) {
    var infoWindow = new google.maps.InfoWindow;
    infoWindow.setContent(title);
    marker.addListener('click', function() {
        infoWindow.open(map, marker);
    });
}

function getPos(callback) {
    if(xmlhttp===undefined) {
        xmlhttp = new XMLHttpRequest();
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            var position = JSON.parse(this.responseText);
            callback.call(null, position);
        }
    };
    xmlhttp.open("GET", "/ajax?t=" + (parseInt(Math.random()*10000000)), true);
    xmlhttp.send();
}


function calculateAndDisplayRoute() {
    if(directionService===undefined)
        return;
    // First, remove any existing markers from the map.
    for (var i = 0; i < markerArray.length; i++) {
        markerArray[i].setMap(null);
    }

    // Retrieve the start and end locations and create a DirectionsRequest using
    // WALKING directions.
    directionService.route({
        origin: remote_marker.getPosition(),
        destination: my_marker.getPosition(),
        travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
        // Route the directions and pass the response to a function to create
        // markers for each step.
        if (status === 'OK') {
            directionDisplay.setDirections(response);
            showSteps(response, markerArray, stepDisplay, map);
        } else {
            console.log('Directions request failed due to ' + status);
        }
    });
}
function showSteps(directionResult, markerArray, stepDisplay, map) {
    var streetPoints = el('#street-points', true);
    if(!streetPoints.checked)
        return;
    // For each step, place a marker, and add the text to the marker's infowindow.
    // Also attach the marker to an array so we can keep track of it and remove it
    // when calculating new routes.
    var myRoute = directionResult.routes[0].legs[0];
    for (var i = 0; i < myRoute.steps.length; i++) {
        var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
        marker.setMap(map);
        marker.setIcon('/images/street-marker.png');
        marker.setPosition(myRoute.steps[i].start_location);
        attachInstructionText(stepDisplay, marker, myRoute.steps[i].instructions, map);
    }
}

function attachInstructionText(stepDisplay, marker, text, map) {
    google.maps.event.addListener(marker, 'click', function() {
        // Open an info window when the marker is clicked on, containing the text
        // of the step.
        stepDisplay.setContent(text);
        stepDisplay.open(map, marker);
    });
}