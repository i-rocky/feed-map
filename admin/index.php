<?php

include $_SERVER['DOCUMENT_ROOT'].'/lib/Config.php';
$config = new Config;

if(!empty($_GET) && isset($_GET['action'], $_GET['secret']) && $_GET['action']==='update-location' && $config->getConfig(Config::SECRET)===$_GET['secret']) {
	$config->setConfig(Config::LAT_KEY, $_GET['lat']);
	$config->setConfig(Config::LNG_KEY, $_GET['lng']);
	print 'ok';
	exit;
}
print "not ok";